@extends('layouts.app')

@section('content')
    <div class="mb-5">
        <h3>MAINTENANCE FEE RECEIPT</h3>
    </div>
    <div class="mb-3">
        <form action="{{ Route('maintenance.create') }}" action="GET">
            <button type="submit" class="btn btn-primary">Create Receipt</button>
        </form>
    </div>
    <table class="table table-light table-hover">
        <thead>
            <tr class="table-primary">
            <th scope="col">No.</th>
            <th scope="col">Date Issued</th>
            <th scope="col">Amount (RM)</th>
            <th scope="col">Date Payment</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($maintenanceFees as $key => $maintenanceFee)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $maintenanceFee->getDateIssued()  }}</td>
                    <td>{{ $maintenanceFee->getAmount() }}</td>
                    <td>{{ $maintenanceFee->getDatePayment() }}</td>
                    <td>
                        <a class="btn btn-warning btn-sm" href="{{ Route('maintenance.edit', $maintenanceFee->id) }}" role="button">Edit</a>
                        <a class="btn btn-danger btn-sm delete" href="" role="button" data-bs-toggle="modal" data-bs-target="#delete_{{ $maintenanceFee->id }}">Delete</a>
                    </td>
                </tr>
                {{-- Modal Delete --}}
                <div class="modal" tabindex="-1" id="delete_{{ $maintenanceFee->id }}" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Delete Confirmation</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="{{ Route('maintenance.destroy', $maintenanceFee->id) }}" method="POST" id="deleteReceipt">
                            @csrf
                            @method('DELETE')
                            <div class="modal-body text-center">
                                <input type="hidden" name="" id="" value="{{ $maintenanceFee->id }}">
                                <p>Delete this data?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>
    <div class="pagination justify-content-end">
        {{ $maintenanceFees->links() }} 
    </div>


@endsection

