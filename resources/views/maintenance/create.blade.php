@extends('layouts.app')

@section('content')
    <div class="mb-5">
        <h3>CREATE MAINTENANCE FEE RECEIPT</h3>
    </div>
    <div class="col-md-4">
        <form action="{{ Route('maintenance.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputDateIssued" class="col-form-label">Date Issued *</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control @error('date_issued') is-invalid @enderror" name="date_issued" id="datepicker" />
                    @error('date_issued')
                        <p style="font-size: 13px; color:red">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputAmount" class="col-form-label">Amount *</label>
                </div>
                <div class="col-md-6">
                  <input type="number" step=".01" name="amount" id="inputAmount" class="form-control @error('amount') is-invalid @enderror">
                  @error('amount')
                        <p style="font-size: 13px; color:red">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputDatePayment" class="col-form-label">Date Payment *</label>
                </div>
                <div class="col-md-6">
                    <input type="date" name="date_payment" id="inputDatePayment" class="form-control  @error('date_payment') is-invalid @enderror">
                    @error('date_payment')
                        <p style="font-size: 13px; color:red">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                    <label for="formFile" class="form-label">Receipt</label>
                </div>
                <div class="col-md-6">
                    <input class="form-control @error('receipt') is-invalid @enderror" name="receipt" type="file" id="formFile" required>
                    @error('receipt')
                    <p style="font-size: 13px; color:red">{{ $message }}</p>
                @enderror
                </div>
            </div>
            <div class="row g-3 align-items-end mb-3">
                <div class="col-md-9 text-end">
                    <a class="btn btn-secondary btn-sm" href="{{ Route('maintenance.index') }}" role="button">Cancel</a>
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </form>
    </div>  
@endsection

