@extends('layouts.app')

@section('content')
    <div class="mb-5">
        <h3>EDIT MAINTENANCE FEE RECEIPT</h3>
    </div>
    <div class="col-md-4">
        <form action="{{ Route('maintenance.update', $maintenance_fee->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputDateIssued" class="col-form-label">Date Issued *</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control @error('date_issued') is-invalid @enderror" name="date_issued" id="datepicker" value="{{ $maintenance_fee->getDateIssued() }}" />
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputAmount" class="col-form-label">Amount *</label>
                </div>
                <div class="col-md-6">
                  <input type="number" step=".01" name="amount" id="inputAmount" class="form-control @error('amount') is-invalid @enderror" value="{{ $maintenance_fee->amount }}" >
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                  <label for="inputDatePayment" class="col-form-label">Date Payment *</label>
                </div>
                <div class="col-md-6">
                  <input type="date" name="date_payment" id="inputDatePayment" class="form-control  @error('date_payment') is-invalid @enderror" value="{{ $maintenance_fee->date_payment }}" >
                </div>
            </div>
            <div class="row g-3 align-items-center mb-3">
                <div class="col-md-3">
                    <label for="formFile" class="form-label">Receipt</label>
                </div>
                <div class="col-md-6">
                    <input class="form-control" name="receipt" type="file" id="formFile" required>
                </div>
            </div>
            <div class="row g-3 align-items-end mb-3">
                <div class="col-md-9 text-end">
                    <a class="btn btn-secondary btn-sm" href="{{ Route('maintenance.index') }}" role="button">Cancel</a>
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </form>
        
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $("#datepicker").datepicker({
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        });
    </script>
    
   
@endsection

