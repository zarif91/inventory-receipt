<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" >

    {{-- Font Awesome CSS --}}
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" >
 
    {{-- Datepicker CSS --}}
    <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet" >

    {{-- Toastr Requirements --}}
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" >
    <script src="{{ asset('js/toastr.min.js') }}"></script>

    <title>Receipt Inventory</title>
  </head>
  <body>
    @include('layouts.navbar')

     
    <div class="conatiner-fluid">
      <div class="row mt-3">
        <div class="col-md-2 px-4">
          @include('layouts.sidebar')
        </div>

        <div class="col-md-10 px-4">
            @yield('content')
        </div>
      </div>
    </div>

    @include('layouts.toastrscript')
    
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

    {{-- For datepicker --}}
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script>
      $("#datepicker").datepicker({
      format: "mm-yyyy",
      startView: "months", 
      minViewMode: "months",
      });
    </script>
    {{-- end for datepicker --}}

    
  </body>
</html>