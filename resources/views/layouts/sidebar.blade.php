
<div class="list-group mb-3">
    <a href="#" class="list-group-item list-group-item-action disabled text-center" tabindex="-1" aria-disabled="true">HOUSE</a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-landmark"></i>
        Loan
    </a>
    <a href="{{ Route('maintenance.index') }}" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-wrench"></i>
        Maintenance Fee
    </a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-percent"></i>
        Cukai Taksiran
    </a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-hand-holding-usd"></i>
        Cukai Tanah
    </a>   
</div>
<div class="list-group">
    <a href="#" class="list-group-item list-group-item-action disabled text-center" tabindex="-1" aria-disabled="true">CAR</a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-car"></i>
        Road Tax
    </a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-shield-alt"></i>
        Insurance
    </a>
    <a href="#" class="list-group-item list-group-item-action" style="border: none">
        <i class="fas fa-concierge-bell"></i>
        Service
    </a>
</div>