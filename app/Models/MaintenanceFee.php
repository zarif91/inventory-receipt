<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceFee extends Model
{
    use HasFactory;

    protected $table = 'maintenance_fees';

    protected $primaryKey = 'id';

    protected $fillable = ['date_issued','amount','date_payment','receipt'];

    public function getDateIssued()
    {
        return \Carbon\Carbon::parse($this->date_issued)->format('m-Y');
    }

    public function setDateIssuedAttribute($value)
    {
        $this->attributes['date_issued'] = \Carbon\Carbon::parse('01'.'-'.$value)->format('Y-m-d');
    }

    public function getDatePayment()
    {
        return \Carbon\Carbon::parse($this->date_payment)->format('d-m-Y');
    }

    public function getAmount()
    {
        return number_format($this->amount,2);
    }

}
