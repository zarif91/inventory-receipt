<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMaintenanceFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_issued' => 'required',
            'amount' => 'required|integer|min:0',
            'date_payment' => 'required',
            'receipt' => 'mimes:jpg,jpeg,png,pdf',
        ];
    }
}
