<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMaintenanceFeeRequest;
use Illuminate\Http\Request;
use App\Models\MaintenanceFee;
use App\Services\MaintenanceFeeService;


class MaintenanceFeeController extends Controller
{
    
    public function index()
    {
        $maintenanceFees = MaintenanceFee::paginate(3);
        return view('maintenance.index')->with(compact('maintenanceFees'));
    }

    
    public function create()
    {
        return view('maintenance.create');
    }
    
    public function store(StoreMaintenanceFeeRequest $request, MaintenanceFeeService $maintenanceFeeService)
    {
        $maintenance = MaintenanceFee::create([
            'date_issued' => $request->date_issued,
            'amount' => $request->amount,
            'date_payment' => $request->date_payment,
            'receipt' => $maintenanceFeeService->storePdf($request->date_issued, $request->receipt),
        ]);
 
        return redirect()->route('maintenance.index')->with('message','Data added Successfully');
    }
    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $maintenance_fee = MaintenanceFee::findOrFail($id);
        return view('maintenance.edit', compact('maintenance_fee'));
    }
  
    public function update(StoreMaintenanceFeeRequest $request, $id, MaintenanceFeeService $maintenanceFeeService)
    {
        $maintenance = MaintenanceFee::findOrFail($id)->update([
            'date_issued' => $request->date_issued,
            'amount' => $request->amount,
            'date_payment' => $request->date_payment,
            'receipt' => $maintenanceFeeService->updatePdf($id, $request->date_issued, $request->receipt),
        ]);
        return redirect()->route('maintenance.index')->with('message','Data edited Successfully');
    }

    public function destroy($id)
    {
        MaintenanceFee::findOrFail($id)->destroy($id);

        return redirect()->route('maintenance.index')->with('message','Data deleted Successfully');
    }
}
