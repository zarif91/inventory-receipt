<?php

namespace App\Services;

use App\Models\MaintenanceFee;

class MaintenanceFeeService{

    public function storePdf($date_issued, $receipt)
    {
        $name_pdf = 'MF'.'_'.$date_issued.'.'.$receipt->extension();
        $set_path = $receipt->move(storage_path('app/public/maintenance'), $name_pdf);

        return $name_pdf;
    }

    public function updatePdf($id, $date_issued, $receipt)
    {
        if(MaintenanceFee::whereId($id)->value('receipt') != null){
            $getPreviousFile = MaintenanceFee::whereId($id)->value('receipt');
            $path = storage_path('app/public/maintenance/'.$getPreviousFile);
            unlink($path);
        }
        $name_pdf = 'MF'.'_'.$date_issued.'.'.$receipt->extension();
        $set_path = $receipt->move(storage_path('app/public/maintenance'), $name_pdf);

        return $name_pdf;
    }
}